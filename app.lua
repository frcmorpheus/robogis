
for _, v in pairs({
    "./util.lua",
    "./handlers.lua"
}) do dofile(v) end

-- define new handlers in handlers.lua, or anonymously here
route_table = {
    ["/api/hello"] = handlers.hello,
    ["/api/rot13"] = handlers.rot,
    ["/api/submit"] = handlers.submit,
    ["/api/ip"] = handlers.ip,
    ["/api/hash"] = handlers.hash
}

function route(ctx)
    local f = route_table[ctx.REQUEST_URI]
    if not f then
        f = function()
            return 200,
                   { ["Content-type"] = "text/plain" },
                   coroutine.wrap(function()
                        coroutine.yield("no handler for "..ctx.REQUEST_URI)
                   end)
        end
    end
    return f(ctx)
end

return route
