handlers = {
--
    hello = function(ctx)
        local h = function()
            coroutine.yield("Hello World!")
        end
        return 200,
               { ["Content-Type"] = "text/plain" },
               coroutine.wrap(h)
    end,
--
    rot = function(ctx)
        local data = ctx.input.read()
        local h = function()
            if not data then
                coroutine.yield("nil")
            else
                coroutine.yield(util.rot13(data))
            end
        end
        return 200,
               { ["Content-Type"] = "text/plain" },
               coroutine.wrap(h) 
    end,
--
    ip = nil, -- unimplemented
--
    hash = function(ctx)
        local data = ctx.input.read()
        local h = function()
            if not data then
                coroutine.yield("nil")
            else
                coroutine.yield(util.hash(data))
            end
        end
        return 200,
               { ["Content-Type"] = "text/plain" },
               coroutine.wrap(h) 
    end,
--
    submit = function(ctx)
        local data = ctx.input.read()
        local h = function()
            if not data then
                coroutine.yield("nil")
            else
                local db = assert(io.open("data/"..util.hash(data), "wb"))
                db:write(data) db:close()
                coroutine.yield("ok")
            end
        end
        return 200,
               { ["Content-Type"] = "text/plain" },
               coroutine.wrap(h) 
    end
--
}

