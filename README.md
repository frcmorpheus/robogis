# RoboGIS

Simply put, this is a Lua uWSGI applet intended to accept SQLite databases
via HTTP/1.1 POST and merge them into a single database for subsequent
retrieval and analysis.

It uses a sketchy content framework of my own design, during the development
of which I authored a few other (mostly useless) features which are still
present in RoboGIS. You can disable them by removing their entries in the
routing table (which you can find in `app.lua`).


## Installation

Installation isn't exactly straightforward – to make it work properly you'll
need LuaSQLite3, the Kepler Project md5 module, and a Lua engine of some kind.
You can use PUC or LuaJIT, but make sure to link everything to the same runtime
or the ABI won't match.

Of course, you'll need uWSGI (or another WSGI-compliant Lua app server, I
suppose) in order to provide the execution environment. RoboGIS can be
run under lua-fcgi with some minor code modifications, but it is far less
secure.

RoboGIS (as of the time of writing) expects to find `core.so` and `lsqlite3.so`
in the app root directory. Keeping them in the system-wide Lua path is also OK,
but you'll need to adjust the package.loadlib call in `util.lua` accordingly.


## Running under uWSGI

For development purposes, this command typically does the trick:
```
uwsgi --http :8080 --http-modifier1 6 --lua app.lua

```
If you're doing anything remotely important, you'll want to set up multiple
workers and put them behind a web server that speaks the uWSGI protocol (like
nginx). See uWSGI's documentation for information on binding to uWSGI protocol
sockets.


## Development Workflow / Style Guide

New changes are made to the `devel` branch, and merged into `master` once
feature-complete and deemed (more or less) stable. Ideally, code checked
out of the `master` branch should never be broken.

 - Use spaces instead of tabs. Each indentation "level" should be four spaces
   wide unless extra spaces are needed for geometric consistency.

 - If applicable, function calls should be tangent to their arguments, i.e. 
   `getuid()`. 

 - If applicable, loops and other control flow structures should *not* be
   tangent to their parameters, i.e. `for (int i = 0; i <= 9; i++)`.

 - If applicable, place brackets immediately after their accompanying 
   statement, not on the next line.

 - If applicable, use `for (;;)` instead of `while (true)` or `while (1)`.

