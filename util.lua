util = {}

util.md5 = package.loadlib("./md5.so", "luaopen_md5_core")()

function util.dump(obj, x_sep, x_seen)
    sep = x_sep or ""
    seen = x_seen or {}
    seen[obj] = true
    local s = {} n = 0 
    for k in pairs(obj) do
        n = n + 1 s[n] = k 
    end 
    table.sort(s)
    for _,v in ipairs(s) do
        print(sep, v)
        v = obj[v]
        if type(v) == "table" and not seen[v] then
            util.dump(v, sep.."\t", seen)
        end 
    end 
end

function util.rot13(str)
    local a = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    local b = "NOPQRSTUVWXYZABCDEFGHIJKLMnopqrstuvwxyzabcdefghijklm"
    return (str:gsub("%a", function(c) return b:sub(a:find(c)) end))
end

function util.hash(k)
    k = util.md5.sum(k)
    return (string.gsub(k, ".", function(c)
                return string.format("%02x", string.byte(c))
            end))
end
